query.selectFrom(TABLIKE.join(TABUSER)
	.on(TABLIKE.USERID.eq(TABUSER.USERID)))
	.where(TABLIKE.PRODUCTID.eq(1337))
	.and(TABUSER.CLUSTERID.eq(42))
	.orderBy(TABLIKE.IDLIKE.asc())
	.fetch();